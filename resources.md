---
layout: default
title: Talks
permalink: /resources/
order: 50
---

# Talks & Resources

## Contribute

Find out how to contribute on our [Contribute]({{ "/contribute/" | relative_url }}) page.

## Talks

{% for x in site.data.presentations %}

{% assign slug = x.title | append: "-" | append: x.event.date | slugify %}

<article>
<h3 id="{{slug}}">{{x.title}}</h3>

<a class="font-weight-bold text-secondary"  href="{{x.event.url}}" >{{x.event.name}} • {{ x.event.location }}</a>

<p class="my-1">by {{ x.presented_by }}
{% if x.event.date %}
 on {{ x.event.date }}
{% endif %}
</p>

<section>

{% if x.video.youtube %}
<a class="btn btn-sm btn-outline-primary" href="https://www.youtube.com/watch?v={{ x.video.youtube }}">YouTube</a>
{% endif %}

{% if x.video.url %}
<a class="btn btn-sm btn-outline-primary "
href="{{ x.video.url }}">Video</a>
{% endif %}

{% if x.video.subtitles %}
<a class="btn btn-sm btn-outline-primary" href="{{ x.video.subtitles }}">Subtitles</a>
{% endif %}

{% if x.slides %}
<a class="btn btn-sm btn-outline-primary" href="{{ x.slides }}">Slides</a>
{% endif %}

</section>
</article>
{% endfor %}

## Slides

We store all of our presentations in PDF or HTML form
[here](/_lfs/presentations). We also have [the original sources available in Git](https://salsa.debian.org/reproducible-builds/reproducible-presentations).
