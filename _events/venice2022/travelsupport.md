---
layout: default
title: Venice 2022 - Travel Bursary
permalink: /events/venice2022/travelsupport
event_hide: true
event_date: 2022-11-01
event_date_string: November 1st-3rd 2022
event_location: Venice, Italay
---

# {{ page.title }}

**<a href="{{ "/events/venice2022" | relative_url }}">← Main event page</a>**

### Instructions for requesting travel and/or accommodation sponsorship

If you are seeking sponsorship to attend this event, please email <a href="mailto:2022-summit-team@lists.reproducible-builds.org">2022-summit-team@lists.reproducible-builds.org</a> with these details:

    I'm seeking sponsorship for:
    [ ] food/meals
    [ ] accommodation
    [ ] travel
        * I'm travelling from ____ via _____ (main mode of transportation)
        * When going back I'll go to _____ via ____ (main mode of transportation)
        * I foresee the expense will be ______ €

If you ask for sponsored accommodation expect us to organize your room on your behalf (if this won't work out, we will inform you well in advance), however we will ask you to pay for it if you later don't attend and the cancellation deadline is passed.
For the sponsored travel, you will need to pay the travel from your own pocket and you will be reimbursed *after* the event.

You will receive confirmation of the bursary by September 12th 2022.
The budget for sponsored attendance is limited, so please do send your request early.


If you'd rather not have such information recorded on a mailing list, please send the email directly to <a href="mailto:mattia@reproducible-builds.org">Mattia</a> instead.
